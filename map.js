var map;

var tivoli = {
  lat: 55.67405678196548,
  lng: 12.568788199999972,
};

var sydney={
        lat: -33.8688,
        lng: 151.2195
  };

var markers = [];
var markers_SV=[];
var all_overlays = [];
var headings=[];
var lats=[];
var lngs=[];
var searchManager;

localStorage.clear();

//--------------------------------------------------------------------
function openInNewTab(map) {

  localStorage.clear();

  var mapSettings = {
    mapTypeId: Microsoft.Maps.MapTypeId.aerial,
    showZoomButtons:false,
    allowHidingLabelsOfRoad: true,
    showLocateMeButton: false,
    showMapTypeSelector:false,
    center: map.getCenter(),
    customMapStyle:{"elements":{
      "area":{"visible": false,"labelVisible":false},
      "point":{"visible":false,"labelVisible":false},
      "political":{"visible":false,"labelVisible":false},
      "structure":{"visible":false,"labelVisible":false},
      "transportation":{"visible":false,"labelVisible":false},
      "water":{"visible":false,"labelVisible":false},
      "transit":{"visible":false,"labelVisible":false}
    }
    }
  };

  //getheadings();

  current_lng= map.getCenter();
  current_lat= map.getCenter();

  localStorage.setItem('mapSettings', JSON.stringify(mapSettings));
  localStorage.setItem('current_lng',JSON.stringify(current_lng));
  localStorage.setItem('current_lat',JSON.stringify(current_lat));

  //open the new tab which loads the json settings
  //cannot directly call new google maps link with correct settings because variables cant seem to be added to the non-static image ur/
  var win = window.open('pre_export.html', 'Export');
  win.focus();
}
//------------------------------------------------------------------------------------------------------------------


//DROPDOWN
//Sets the zoom level depending on the dropdown
function listQ() {
  var e = document.getElementById("select");
  if ("world3" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":3});
  } else if ("world4" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":4});
  } else if ("country5" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":5});
  } else if ("country6" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":6});
  } else if ("state" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":7});
  } else if ("region8" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":8});
  } else if ("region9" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":9});
  } else if ("region10" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":10});
  } else if ("region11" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":11});
  } else if ("city12" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":12});
  } else if ("city13" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":13});
  } else if ("city14" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":14});
  } else if ("city15" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":15});
  } else if ("city16" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":16});
  } else if ("city17" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":17});
  } else if ("site" === e.options[e.selectedIndex].value) {
    map.setView({"zoom":18});
  }
}


document.getElementById("select").addEventListener("click", listQ);


//CENTER MAP BUTTONS
function CenterControl(controlDiv, map, center) {
  var control = this;

  // Set the center property upon construction
  control.center_ = center;
  controlDiv.style.clear = 'both';

  // Set CSS for the control border
  var goCenterUI = document.createElement('div');
  goCenterUI.id = 'goCenterUI';
  goCenterUI.title = 'Click to recenter the map';
  controlDiv.appendChild(goCenterUI);

  // Set CSS for the control interior
  var goCenterText = document.createElement('div');
  goCenterText.id = 'goCenterText';
  goCenterText.innerHTML = 'Center Map';
  goCenterUI.appendChild(goCenterText);

  // Set CSS for the setCenter control border
  var setCenterUI = document.createElement('div');
  setCenterUI.id = 'setCenterUI';
  setCenterUI.title = 'Click to change the center of the map';
  controlDiv.appendChild(setCenterUI);

  // Set CSS for the control interior
  var setCenterText = document.createElement('div');
  setCenterText.id = 'setCenterText';
  setCenterText.innerHTML = 'Set Center';
  setCenterUI.appendChild(setCenterText);

  // Set up the click event listener for 'Center Map': Set the center of
  // the map
  // to the current center of the control.
  goCenterUI.addEventListener('click', function() {
    var currentCenter = control.getCenter();
    map.setCenter(currentCenter);
  });

  // Set up the click event listener for 'Set Center': Set the center of
  // the control to the current center of the map.
  setCenterUI.addEventListener('click', function() {
    var newCenter = map.getCenter();
    control.setCenter(newCenter);
  });
}


//BORDERS BUTTON
function bordersControl(bordersDiv, map) {
  var control = this;

  // Set the center property upon construction
  control.center_ = center;
  bordersDiv.style.clear = 'both';

  // Set CSS for the control border
  var bordersUI = document.createElement('div');
  bordersUI.id = 'goCenterUI';
  bordersUI.title = 'Click to recenter the map';
  bordersDiv.appendChild(bordersUI);

  // Set CSS for the control interior
  var bordersText = document.createElement('div');
  bordersText.id = 'goCenterText';
  bordersText.innerHTML = 'Borders';
  bordersUI.appendChild(bordersText);

  // Set up the click event listener for 'Set Center': Set the center of
  // the control to the current center of the map.
  bordersUI.addEventListener('click', function() {
    var newCenter = map.getCenter();
    control.setCenter(newCenter);
  });
};


// /**
//  * Define a property to hold the center state.
//  * @private
//  */
CenterControl.prototype.center_ = null;

// /**
//  * Gets the map center.
//  * @return {?google.maps.LatLng}
//  */
CenterControl.prototype.getCenter = function() {
  return this.center_;
};

// /**
//  * Sets the map center.
//  * @param {?google.maps.LatLng} center
//  */
CenterControl.prototype.setCenter = function(center) {
  this.center_ = center;
};


//-------------------------------------------------------------------------------------------------------------------------------
function initAutocomplete() {
  map = new Microsoft.Maps.Map(document.getElementById('map'), {
      /* No need to set credentials if already passed in URL */
      center: new Microsoft.Maps.Location(tivoli.lat, tivoli.lng),
      mapTypeId: Microsoft.Maps.MapTypeId.aerial,
      zoom: 10,
      allowHidingLabelsOfRoad: true,
      showMapTypeSelector:false,
      showZoomButtons:false,
      showLocateMeButton:false,
      customMapStyle:{"elements":{
        "area":{"visible": false,"labelVisible":false},
        "point":{"visible":false,"labelVisible":false},
        "political":{"visible":false,"labelVisible":false},
        "structure":{"visible":false,"labelVisible":false},
        "transportation":{"visible":false,"labelVisible":false},
        "water":{"visible":false,"labelVisible":false},
        "transit":{"visible":false,"labelVisible":false},
        }
      }
      });



  // map = new google.maps.Map(document.getElementById('map'), {
  //   center: tivoli,
  //   mapTypeId: google.maps.MapTypeId.SATELLITE,
  //   zoom: 12,
  //   mapTypeControlOptions: {
  //     style: google.maps.MapTypeControlStyle.DEFAULT,
  //     mapTypeIds: ['roadmap', 'terrain', 'satellite']
  //   },
  //   styles: [{
  //       "featureType": "administrative",
  //       "elementType": "geometry",
  //       "stylers": [{
  //         "visibility": "off"
  //       }]
  //     },
  //     {
  //       "featureType": "administrative.land_parcel",
  //       "stylers": [{
  //         "visibility": "off"
  //       }]
  //     },
  //     {
  //       "featureType": "administrative.neighborhood",
  //       "stylers": [{
  //         "visibility": "off"
  //       }]
  //     },
  //     {
  //       "featureType": "poi",
  //       "stylers": [{
  //         "visibility": "off"
  //       }]
  //     },
  //     {
  //       "featureType": "road",
  //       "stylers": [{
  //         "visibility": "off"
  //       }]
  //     },
  //     {
  //       "featureType": "road",
  //       "elementType": "labels",
  //       "stylers": [{
  //         "visibility": "off"
  //       }]
  //     },
  //     {
  //       "featureType": "road",
  //       "elementType": "labels.icon",
  //       "stylers": [{
  //         "visibility": "off"
  //       }]
  //     },
  //     {
  //       "featureType": "transit",
  //       "stylers": [{
  //         "visibility": "off"
  //       }]
  //     },
  //     {
  //       "featureType": "water",
  //       "elementType": "labels.text",
  //       "stylers": [{
  //         "visibility": "off"
  //       }]
  //     }
  //   ]
  // });




//   // Create the search box and link it to the UI element.

//   map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
//
//   // Bias the SearchBox results towards current map's viewport.
//   map.addListener('bounds_changed', function() {
//     searchBox.setBounds(map.getBounds());
//   });
//
//
//   // Listen for the event fired when the user selects a prediction and retrieve
//   // more details for that place.
//   searchBox.addListener('places_changed', function() {
//     var places = searchBox.getPlaces();
//
//     if (places.length == 0) {
//       return;
//     }
//
//     // Clear out the old markers.
//     markers.forEach(function(marker) {
//       marker.setMap(null);
//     });
//     markers = [];
//
//     // For each place, get the icon, name and location.
//     var bounds = new google.maps.LatLngBounds();
//     places.forEach(function(place) {
//       if (!place.geometry) {
//         console.log("Returned place contains no geometry");
//         return;
//       }
//
//       var icon = {
//         url: place.icon,
//         size: new google.maps.Size(71, 71),
//         origin: new google.maps.Point(0, 0),
//         anchor: new google.maps.Point(17, 34),
//         scaledSize: new google.maps.Size(25, 25)
//       };
//
//
//       var whitecircle = {
//         path: 'M2,12C2,6.48,6.48,2,12,2s10,4.48,10,10s-4.48,10-10,10S2,17.52,2,12z M12,18c3.31,0,6-2.69,6-6s-2.69-6-6-6s-6,2.69-6,6 S8.69,18,12,18z',
//         fillColor: 'white',
//         fillOpacity: 1,
//         scale: 1,
//         strokeColor: 'white',
//         strokeWeight: 1,
//         anchor: new google.maps.Point(12, 12)
//       };
//
//       // Create a marker for each place.
//       markers.push(new google.maps.Marker({
//         map: map,
//         icon: whitecircle,
//         title: place.name,
//         position: place.geometry.location,
//         draggable: true
//       }));
//
//       if (place.geometry.viewport) {
//         // Only geocodes have viewport.
//         bounds.union(place.geometry.viewport);
//       } else {
//         bounds.extend(place.geometry.location);
//       }
//     });
//     map.fitBounds(bounds);
//   });
//
//
//   var centerControlDiv = document.createElement('div');
//   var centerControl = new CenterControl(centerControlDiv, map, cph);
//
//   centerControlDiv.index = 1;
//   centerControlDiv.style['padding-top'] = '10px';
//   map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
//
//
//
//
//   //if the center of the map changes then move the white marker
//   map.addListener('center_changed', function() {
//     for (var i = 0; i < markers.length; i++) {
//       markers[i].setPosition(map.getCenter());
// }
//   });
//
//
//
//
//   // This event listener will call addMarker() when the map is clicked.
//   map.addListener('click', function(event) {
//     addMarker(event.latLng);
//   });

}
//---------------------------------------------------------------------------------------------------------------

var input = document.getElementById('pac-input');
//  var searchBox = new google.maps.places.SearchBox(input);

function Search() {
     if (!searchManager) {
         //Create an instance of the search manager and perform the search.
         Microsoft.Maps.loadModule('Microsoft.Maps.Search', function () {
             searchManager = new Microsoft.Maps.Search.SearchManager(map);
             Search()
         });
     } else {
         //Remove any previous results from the map.
         map.entities.clear();
         map.layers.clear();
         //Get the users query and geocode it.
         //var query = document.getElementById('searchTbx').value;
         geocodeQuery(input.value);
     }
 }
 var defaultColor = 'blue';
     var hoverColor = 'red';
     var mouseDownColor = 'purple';

 function geocodeQuery(query) {
     var searchRequest = {
         where: query,
         callback: function (r) {
             if (r && r.results && r.results.length > 0) {
                 var pin, pins = [], locs = [], output = 'Results:<br/>';

                 var layer = new Microsoft.Maps.Layer();
                 map.layers.insert(layer);
                 var hoverLayer = new Microsoft.Maps.Layer();
                 map.layers.insert(hoverLayer);

                 for (var i = 0; i < r.results.length; i++) {
                     //Create a pushpin for each result.
                     pin = new Microsoft.Maps.Pushpin(r.results[i].location, {
                         text: i + ''
                     });
                     pins.push(pin);
                     locs.push(r.results[i].location);
                     output += i + ') ' + r.results[i].name + '<br/>';
                 }
                 //Add the pins to the map
                 //map.entities.push(pins);
                 hoverLayer.add(pins)
                 //Display list of results
                 document.getElementById('output').innerHTML = output;
                 //Determine a bounding box to best view the results.
                 var bounds;
                 if (r.results.length == 1) {
                     bounds = r.results[0].bestView;
                 } else {
                     //Use the locations from the results to calculate a bounding box.
                     bounds = Microsoft.Maps.LocationRect.fromLocations(locs);
                 }
                 map.setView({ bounds: bounds });

                 Microsoft.Maps.Events.addHandler(pin, 'mouseover', function (e) {
                             e.target.setOptions({ color: hoverColor });
                             //Move pin to hover layer.
                             layer.remove(pin);
                             hoverLayer.add(pin);
                         });
                         Microsoft.Maps.Events.addHandler(pin, 'mousedown', function (e) {
                             e.target.setOptions({ color: mouseDownColor });
                            map.setView({"zoom":17,"center":e.target.getLocation()});
                            //map1.setView({"zoom":5});
                         });
                         Microsoft.Maps.Events.addHandler(pin, 'mouseout', function (e) {
                             e.target.setOptions({ color: defaultColor });
                             //Move pin to main layer.
                             hoverLayer.remove(pin);
                             layer.add(pin);
                         });

             }
         },
         errorCallback: function (e) {
             //If there is an error, alert the user about it.
             alert("No results found.");
         }
     };
     //Make the geocode request.
     searchManager.geocode(searchRequest);
 }



//---------------------------------------------------------------------------------------------------------------
//FUNCTIONS

//print current position in a div
// function getCurrent(map) {
//   var currentPosition = map.getCenter();
//   document.getElementById("demo").innerHTML = currentPosition;
// }


// Adds a marker to the map and push to the array.
function addMarker(location) {
  var marker = new google.maps.Marker({
    position: location,
    map: map
  });
  markers_SV.push(marker);
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (var i = 0; i < markers_SV.length; i++) {
    markers_SV[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}

// Shows any markers currently in the array.
function showMarkers() {
  setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers_SV = [];
}

function printlatlng() {
  for (var i = 0; i < markers_SV.length; i++) {
    markers_SV[i].setMap(map);
    var newDiv = document.createElement("div");
    var newContent = document.createTextNode(markers_SV[i].getPosition().lat(),markers_SV[i].getPosition().lng());
    newDiv.appendChild(newContent);
    document.body.appendChild(newDiv);
  }
}

function deleteAllShape() {
  for (var i=0; i < all_overlays.length; i++)
  {
    all_overlays[i].overlay.setMap(null);
  }
  all_overlays = [];
}

function getheadings(){
//given the location af the marker and the location of the site, calculate the heading vector

for (var i = 0; i < markers_SV.length; i++) {
  markers_SV[i].setMap(map);

  var lat_d=markers_SV[i].getPosition().lat();
  var lat_o=map.getCenter().lat();
  var lng_d=markers_SV[i].getPosition().lng();
  var lng_o=map.getCenter().lng();

  var y = Math.sin(lng_o-lng_d) * Math.cos(lat_o);
  var x = Math.cos(lat_d)*Math.sin(lat_o) - Math.sin(lat_d)*Math.cos(lat_o)*Math.cos(lng_o-lng_d);
  var brng = ((Math.atan2(y, x)*(180/Math.PI))+360)%360;


  headings.push(brng);
  lats.push(lat_d);
  lngs.push(lng_d);

  // var newDiv = document.createElement("div");
  // var newContent = document.createTextNode(brng);
  // newDiv.appendChild(newContent);
  // document.body.appendChild(newDiv);
}


localStorage.setItem('headings', JSON.stringify(headings));
localStorage.setItem('lats', JSON.stringify(lats));
localStorage.setItem('lngs', JSON.stringify(lngs));

// var newDiv = document.createElement("div");
// var newContent = document.createTextNode(headings.length);
// newDiv.appendChild(newContent);
// document.body.appendChild(newDiv);
}
