
// var tivoli = {
//   lat: 55.67405678196548,
//   lng: 12.568788199999972,
// };

// import html2canvas from 'html2canvas'

var panorama;
var panoramas=[];
var pano_IDs=[];
var map;
var latlng;

//load data passed from index.html
//var headings = JSON.parse(localStorage.getItem("headings"));
var current_lat = JSON.parse(localStorage.getItem("lats"));
var current_lng = JSON.parse(localStorage.getItem("lngs"));


function initialize() {
  initMap();
}

function initMap() {
  var imported_settings = localStorage.getItem('mapSettings');
  //console.log('Map Settings: ' + JSON.parse(imported_settings));

  map1 = new Microsoft.Maps.Map(document.getElementById('map1'),JSON.parse(imported_settings));
  map1.setView({"zoom":5});

  map2 =  new Microsoft.Maps.Map(document.getElementById('map2'), JSON.parse(imported_settings));
  map2.setView({"zoom":8});

  map3 = new Microsoft.Maps.Map(document.getElementById('map3'), JSON.parse(imported_settings));
  map3.setView({"zoom":11});

  map4 = new Microsoft.Maps.Map(document.getElementById('map4'), JSON.parse(imported_settings));
  map4.setView({"zoom":14});

  map5 = new Microsoft.Maps.Map(document.getElementById('map5'), JSON.parse(imported_settings));
  map5.setView({"zoom":17});
  //
  // map6 = new Microsoft.Maps.Map(document.getElementById('map6'), JSON.parse(imported_settings));
  // map6.setZoom(18);
  // map6.setTilt(45);
  //
  // map7 = new Microsoft.Maps.Map(document.getElementById('map7'), JSON.parse(imported_settings));
  // map7.setZoom(18);
  // map7.setTilt(45);
  // map7.setHeading(90);
  //
  // map8 = new Microsoft.Maps.Map(document.getElementById('map8'), JSON.parse(imported_settings));
  // map8.setZoom(18);
  // map8.setTilt(45);
  // map8.setHeading(180);
  //
  // map9 = new Microsoft.Maps.Map(document.getElementById('map9'), JSON.parse(imported_settings));
  // map9.setZoom(18);
  // map9.setTilt(45);
  // map9.setHeading(270);
}
